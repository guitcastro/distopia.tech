// This is where project configuration and plugin options are located. 
// Learn more: https://gridsome.org/docs/config

// Changes here requires a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'Distopia',
  siteDescription: 'Distopia é uma antítese da utopia, uma realidade não muito distante se continuarmos desvirtuando da razão em prol da barbárie. Neste blog falo sobre tecnologia, marketing, filosofia, descentralização, política, educação, capitalismo, socialismo, fascismo, anarquismo e mais',
  plugins: [
    {
      // Create posts from markdown files
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'Post',
        path: 'content/posts/*.md',
        route: '/:slug',
        refs: {
          // Creates a GraphQL collection from 'tags' in front-matter and adds a reference.
          tags: {
            typeName: 'Tag',
            route: '/tag/:id',
            create: true
          }
        }
      }
    },
    {
      use: 'gridsome-plugin-rss',
      options: {
        contentTypeName: 'Post',
        feedOptions: {
          title: 'Distopia.tech',
          feed_url: 'https://distopia.tech/rss.xml',
          site_url: 'https://distopia.tech'
        },
        feedItemOptions: node => ({
          title: node.title,
          description: node.description,
          url: 'https://distopia.tech/post/' + node.slug,
          author: node.fields.author
        }),
        output: {
          dir: './static',
          name: 'rss.xml'
        }
      }
    }
  ],

  transformers: {
    //Add markdown support to all file-system sources
    remark: {
      externalLinksTarget: '_blank',
      externalLinksRel: ['nofollow', 'noopener', 'noreferrer'],
      anchorClassName: 'icon icon-link',
      plugins: [
        '@gridsome/remark-prismjs'
      ]
    }
  },
}